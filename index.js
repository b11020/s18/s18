console.log("pokemon");
// let myPokemon = {
// 	name: "pikachu",
// 	level: 3,
// 	health: 100,
// 	attack: 50,
// 	tackle: function(){
// 				console.log("this pokemon tackled targetPokemon");
// 				console.log("targetPokemon's health is reduced to newTargetPokemonHealth");
// 	},
let trainer ={};
// initialize trainer object properties
	trainer.name = "ash ketchum";
	trainer.age = 35;
	trainer.pokemon = ["Pikachu", "Ratata", "Squirtle", "Charmander"];
	trainer.friends = {
		hoenn: ["Misty", "Brock"],
		kanto: ["Max", "Dawn"]
};
// initialize ---object method .talk----prints "message pikachu! i choose you!"
trainer.talk = function(){
	console.log(this.pokemon[0] + "! I chooose you!");
}
// 	faint: function(){
// 		console.log("pokemon fainted");
// 	}
// }
// console.log(myPokemon);

// triner object properties using "dot and square bracket notation" 
console.log(trainer);
	console.log("Result of dot notation:");
	console.log(trainer.name);

console.log("Result of square bracket notation:");
	console.log(trainer["pokemon"]);

console.log("Result of talkk method")
	trainer.talk();


// // object constructor notation

// function Pokemon(name, level){

function Pokemon(name,level){
	// 	// properties
// 	this.name = name;
// 	this.level = level;
// 	this.health = level * 2;
// 	this.attack = level;
	this.name = name;
	this.level = level;
	this.health = level *2;
	this.attack = level;

	//method==============//targetPokemon
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		target.health -= this.attack;
		// subtracts health property of target pokemon object
		if (target.health <= 0){
			target.faint();
		}
		else {
			console.log(target.name + "'s health is now reduced to " + target.health);
		}
	}

	this.faint = function(){
		console.log(this.name + " fainted ");
	}
}


// 	   /*add method                 //targetPokemon*/          
// 	this.tackle = function(target){
// 		           pokemonObject            targetPokemon
// 		console.log(this.name + " tackled " + target.name);
// 		// targetHealth - pokemonAttack = newHealth
// 		// 16-16 = 0

// 		console.log("target Pokemon's health is reduced to newTargetPokemonHealth");

// 		// call faint method if the target pokemon's health is less than or equal to zero 

// 		target.faint();
// 	}
// 	this.faint = function(){
// 	}
// }
// let pikachu = new Pokemon("Pikachu", 16);
// /*
// 	health = 32;
// 	attack = 16;

// */

// console.log(pikachu);

// let ratata = new Pokemon("ratata", 8);
// console.log(ratata);
// 		// object 
// // pikachu.tackle(ratata);
// ratata.tackle(pikachu);

let pikachu = new Pokemon("Pikachu", 16);
	console.log(pikachu);

let ratata = new Pokemon("Ratata", 20);
	console.log(ratata);

let squirtle = new Pokemon("Squirtle", 30);
	console.log(squirtle);

pikachu.tackle(ratata);



